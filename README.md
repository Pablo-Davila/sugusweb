# SugusWeb

Repositorio sincronizado con el contenido de la página web de la Asociación SUGUS en [sugus.eii.us.es](sugus.eii.us.es).

Utilizando Hugo y GitLab CI.

## Estructura

### Tema
Actualmente utiliza el tema [hugo-bootstrap](https://github.com/Xzya/hugo-bootstrap) de [Xyza](https://github.com/Xzya)

### Contenido

Existe la carpeta `content` y se divide principalmente en:

*  `content/post/*.md`  para los posts del blog
*  `content/*.md` para otros elementos (navbar sobre todo)

Más detalles en [docs/content.md](docs/content.md)

## ¿Cómo funciona?

Como cualquier otra web creada con Hugo, el contenido de este repositorio es el producto de haber ejecutado `hugo new site` más la configuración del tema.

Hemos configurado GitLab CI para poder generar la web estática cada vez que la rama master recibe un cambio, usando GitLab runners e imágenes de Docker de Hugo.

Más detalles en [docs/ci.md](docs/ci.md)

## Contribuir

### Para añadir un post nuevo

Si has venido a la sala AS43 o te has puesto en contacto con nosotros por correo para publicar una entrada tan solo deberás hacer un merge request con el post siguiendo las instrucciones en [docs/posting.md](docs/posting.md)

### Issues

También existen [issues](https://gitlab.com/sugus_gitlab/sugusweb/issues) donde se proponen cambios y corrigen errores que se detecten.

Nos gustan las contribuciones así que si no sabes por donde empezar puedes filtrar issues por la etiqueta ~good-first-issue

