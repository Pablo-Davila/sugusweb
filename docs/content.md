# Contenido de la web.

## Posts

Las publicaciones del sitio se encuentran dentro de `content/post/` y tienen el
formato de nombre `yyyy-mm-dd-name.md`. Se caracterizan por tener un bloque
delimitado por `---` que se denomina __frontmatter__, aquí se indica
información sobre el post (metadatos).
```
---
title: "¡Estrenamos nueva página web!"
date: 2019-02-08T17:36:32Z
tags: ["anuncio"]
---
```

La fecha del frontmatter es la que determina la fecha que se muestra en la web,
pero con motivo de que los ficheros se muestren ordenados cronológicamente
dentro de la carpeta `content/post` escribimos la fecha en formato
`yyyy-mm-dd` al principio del fichero del post.

## Elementos del navbar

Los elementos del navbar se enumeran dentro de `config.toml`, especificado
dentro de la sección del idioma al que corresponden. Eg. `[language.es]`

Cada sección del navbar se define con la sección `[[languages.es.menu.main]]` y
sus atributos pueden ser:
* url : dirección de la página a mostrar, dentro de `content/`
* name : nombre que se muestra en el navbar
* weight : sirve para dar orden a la lista de elementos, se van mostrando
  primero los elementos de menor peso.

Por ejemplo, para añadir una sección llamada "Listas de Correos", cuya pagina
es `content/lista_correos.md` y queremos que esté a la derecha del todo:
```
  [[languages.es.menu.main]]
    url = "/lista_correos/"
    name = "Listas de Correos"
    weight = 9 # Mayor valor
```

## brand.html

Se encuentra en `layouts/partials/brand.html` y sirve para incluir contenido a
la izquierda del navbar. Principalmente se utiliza para el logo de la
Asociación. 
