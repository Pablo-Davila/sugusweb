---
title: "Hacktoberfest"
date: 2019-10-08T19:34:03Z
tags: ["Hackathon"]
---

{{< image src="/images/2019-10-02.svg" alt="Hacktoberfest" position="center" style="border-radius: 8px; width: 100%" >}}

El próximo Jueves 10 de Octubre organizaremos una sesión conjunta de contribución con motivo del Hacktoberfest. En esta jornada serán bienvenidas tanto personas que quieran comenzar a contribuir a algún proyecto libre como personas que ya tengan soltura con el workflow propio de estos proyectos.

<!--more-->

En este día no realizaremos ninguna charla, si no que nos agruparemos en equipos según intereses y nos ayudaremos los unos a los otros a realizar los primeros pull requests de la jornada. Si a finales de Octubre has realizado al menos 4 pull requests, DigitalOcean te enviará una camiseta celebrando tu contribución!

Para cualquier duda, puedes preguntarnos en Twitter (@sugus_etsii) =)
¡Nos vemos pronto!
