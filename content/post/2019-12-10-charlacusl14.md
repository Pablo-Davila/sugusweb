---
title: "Charla informativa del 14º Concurso de Software Libre"
date: 2019-12-10T11:07:12+01:00
tags: ["charlas"]
---

{{< image src="/images/2019-12-10.png" alt="Charla CUSL" position="center" style="border-radius: 8px; width: 100%" >}}

¡La 14ª edición ya está aquí! ¿Quieres competir con estudiantes de toda España y ganar premios? Ven a la presentación del CUSL y entérate como hacerlo.

<!--more-->

Ya esta abierto el plazo para inscribirse en la decimocuarta edición del Concurso Universitario de Software Libre que se celebra este curso 2019/2020 y en SUGUS queremos acercar el CUSL a los alumnos de la Universidad de Sevilla.

Este jueves organizamos en la Escuela Superior de Ingeniería Informática una presentación donde se explicará todo lo que necesitas saber para participar, desde que es CUSL  hasta cómo participar, qué plazos hay, cómo hay que desarrollar los proyectos, tipos de premios, etc.  Todo ello presentado por nuestro compañero Antonio, colaborador en la presente edición del concurso, y además tendremos con nosotros a participantes y ganadores de pasadas ediciones que nos contarán de primera mano como fue su experiencia participando en el CUSL.

¡Os esperamos! :D

#### Horarios y aula

- Jueves 12 de diciembre de 2019 a las 12:30

- Escuela Superior de Ingeniería Informática

- Aula: A2.14

- Ponente: Antonio Torres Moríñigo, miembro de SUGUS y colaborador del CUSL



#### Más información

- [Sitio oficial del Concurso Universitario de Software Libre](https://www.concursosoftwarelibre.org/1920/)
